public class SortExercise {

	public static void main(String[] args) {
		// PERFORM the following TASKS in sequence

		int[] numList = { 50, 40, 45, 60, 20, 30, 10, 5, 25, 80 };

		// TODO : Task 1 - Use enhanced FOR loop to display in each line the
		// numbers in numList
		
		for (int element : numList) {
			System.out.println(element);
		}

		// TODO : Task 3 - Use Bubble Sort to sort numList
		
		bubbleSort(numList);

		// TODO : Task 4 - Use enhanced FOR loop to display in each line the
		// numbers in numList
		// Copy codes written for Task 1		
		// If Task 2 is done correctly, you should have a sorted list
		System.out.println("BUBBLE SORT");		
		for (int element : numList) {
			System.out.println(element);
		}
		

		int[] numList2 = { 50, 40, 45, 60, 20, 30, 10, 5, 25, 80 };

		// TODO : Task 6 - Use Selection Sort to sort numList2
		// Use enhanced FOR loop to display in each line the numbers in numList2
		
		selectionSort(numList2);
		System.out.println("SELECTION SORT");
		for (int element : numList2) {
			System.out.println(element);
		}
	}

	public static void bubbleSort(int[] list) {
		// TODO : Task 2 - Write codes for Bubble Sort
		for (int i = 0; i < list.length - 1; i++) {
			for (int j = 0; j < list.length - 1; j++) {
				if (list[j] > list[j + 1]) {
					int temp = list[j];
					list[j] = list[j + 1];
					list[j + 1] = temp;
				}
			}
		}
	}

	public static void selectionSort(int[] list) {
		// TODO : Task 5 - Write codes for Selection Sort
		for (int i = 0; i < list.length - 1; i++) {
			int smallest = i;
			for (int j = i; j < list.length; j++) {
				if (list[j] < list[smallest]) {
					smallest = j;
				}
			}
			int temp = list[i];
			list[i] = list[smallest];
			list[smallest] = temp;
		}
		
	}
}