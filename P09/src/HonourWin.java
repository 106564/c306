import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class HonourWin {
	private RecordReader rr;
	private Student[] studentList;
	private DefaultListModel listModel;

	public HonourWin(RecordReader reader) {
		rr = reader;
		listModel = new DefaultListModel();
		doLoadRecords();
		buildUI();
	}

	private void buildUI() {
		JFrame window = new JFrame("Roll of Honours");
		window.setLocation(50, 50);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		window.setLayout(new BorderLayout());

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(createButtonsPanel(), BorderLayout.NORTH);
		panel.add(createListPanel(), BorderLayout.CENTER);

		window.add(panel, BorderLayout.CENTER);

		window.pack();
		window.setVisible(true);
	}

	private JPanel createListPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JList jlist = new JList(listModel);
		JScrollPane scrollPane = new JScrollPane(jlist);
		panel.setBorder(BorderFactory.createTitledBorder("Students"));
		panel.add(scrollPane, BorderLayout.CENTER);
		return panel;
	}

	private JPanel createButtonsPanel() {
		JPanel panel = new JPanel(new FlowLayout());

		panel.add(createLoadButton());
		panel.add(createSortButton());
		panel.add(createTopButton());

		return panel;
	}

	private JButton createTopButton() {
		JButton btnTop = new JButton("Top 5%");
		btnTop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doShowTopStudents();
			}
		});
		return btnTop;
	}

	private JButton createSortButton() {
		JButton btnSort = new JButton("Sort Records");
		btnSort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doSortRecords();
			}
		});
		return btnSort;
	}

	private JButton createLoadButton() {
		JButton btnLoad = new JButton("Load Records");
		btnLoad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doLoadRecords();
				displayRecords();
			}
		});
		return btnLoad;
	}

	private void displayRecords() {
		listModel.clear();

		// TODO : Task 9 - Write a for loop to print all student records
		for (Student s : studentList) {
			listModel.addElement("Name: " + s.getName() + ", " + "GPA: " + s.getGpa());
		}
	
	}

	private void doLoadRecords() {
		// TODO : Task 10 - reload records from students.csv
		studentList = rr.loadStudents();
	}

	private void doSortRecords() {
		// TODO : Task 11 - Sort Student List using Bubble or Selection Sort
		RecordSorter rs = new RecordSorter(studentList);
		//rs.setSortType(SortType.SELECTION);
		rs.sortStudentList();	
		studentList = rs.getStudents();
		displayRecords();
	}

	private void doShowTopStudents() {
		listModel.clear();
		listModel.addElement("ROLL OF HONOUR");
		
		RecordSorter rs = new RecordSorter(studentList);
		rs.sortStudentList();	
		studentList = rs.getStudents();
		
		// TODO : Task 12 - Calculate how many students in Top 5%
		int total = studentList.length;
		int topFivePercent = (total /  100) * 5;
		
		// TODO : Task 13 - Print the Roll of Honour
		for (int i = 0; i < topFivePercent; i ++) {
			listModel.addElement("Name: " + studentList[i].getName() + ", " + "GPA: " + studentList[i].getGpa());
		}

	}

	public static void main(String[] args) throws Exception {
		RecordReader reader = new RecordReader(new File("students.json"));
		new HonourWin(reader);
	}
}
