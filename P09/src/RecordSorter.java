public class RecordSorter {

	private Student[] students;
	private SortType sortType;

	public RecordSorter(Student[] students) {
		sortType = SortType.BUBBLE; // default bubble sort
		this.students = students;
	}

	public void sortStudentList() {
		if (sortType == SortType.BUBBLE) {
			bubbleSort();
		} else if (sortType == SortType.SELECTION) {
			selectionSort();
		}
	} // sortStudentList

	public void bubbleSort() {
		// TODO : Task 7 - Write codes for Bubble Sort
		// Copy codes from Task 2 to here	
		for (int i = 0; i <  students.length; i++) {
			for (int j = students.length - 1; j > i; j--) {
				if (students[j].getGpa() >  students[j - 1].getGpa()) {
					 Student temp =  students[j];
					 students[j] =  students[j - 1];
					 students[j - 1] = temp;
				}
			}
		}
	} // bubbleSort

	public void selectionSort() {
		// TODO : Task 8 - Write codes for Selection Sort
		// Copy codes from Task 5 to here
		for (int i = 0; i <  students.length; i++) {
			int max = i;
			for (int j = students.length - 1; j > i; j--) {
				if (students[j].getGpa() > students[max].getGpa()) {
					max = j;
				}
			}
			Student temp = students[i];
			students[i] = students[max];
			students[max] = temp;
		}
	} // selectionSort

	public Student[] getStudents() {
		return students;
	}

	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}
}