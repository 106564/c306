import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class Student {
	private int id;
	private String name;
	private String school;
	private double gpa;

	@JsonCreator
	public Student(@JsonProperty("id") int id,
			@JsonProperty("name") String name,
			@JsonProperty("school") String school,
			@JsonProperty("gpa") double gpa) {
		this.id = id;
		this.name = name;
		this.school = school;
		this.gpa = gpa;
	} // constructor

	public double getGpa() {
		return gpa;
	}

	public String getName() {
		return name;
	}

	public String getSchool() {
		return school;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return String.format("Student [studNo=%s, name=%s, school=%s, gpa=%s]",
				id, name, school, gpa);
	}

}
