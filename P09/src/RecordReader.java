import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class RecordReader {
	private ObjectMapper mapper;
	private File src;

	public RecordReader(File src) {
		mapper = new ObjectMapper();
		this.src = src;
	}

	public Student[] loadStudents() {
		ArrayList<Student> records;
		try {
			records = mapper.readValue(src,
					new TypeReference<ArrayList<Student>>() {});
			Student slist[] = new Student[records.size()];
			return records.toArray(slist);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
