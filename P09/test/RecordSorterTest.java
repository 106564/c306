import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;


public class RecordSorterTest {
	private Student students[];
	private Student best, better, good;
	private RecordSorter sorter;

	@Before
	public void setup() {
		students = new Student[3];

		best = new Student(1, "best", null, 4.0);
		better = new Student(2, "better", null, 3.5);
		good = new Student(3, "good", null, 3.0);

		students[0] = better;
		students[1] = best;
		students[2] = good;

		sorter = new RecordSorter(students);
	}

	@Test
	public void testBubbleSort() {
		sorter.bubbleSort();

		Student expected[] = {best, better, good};

		assertThat(sorter.getStudents(), is(expected));
	}

	@Test
	public void testSelectionSort() {
		sorter.selectionSort();

		Student expected[] = {best, better, good};

		assertThat(sorter.getStudents(), is(expected));
	}
}
