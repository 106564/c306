
public class Main {
	
	static String[] strings = {"me", "you", "hello", "bye", "rp"};

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for (int i = 0; i < strings.length; i ++) {
			int longest = i;
			for (int j = strings.length - 1; j > i; j--) {
				if (strings[j].length() < strings[longest].length()) {
					longest = j;
				}
			}
			String temp = strings[i];
			strings[i] = strings[longest];
			strings[longest] = temp;
			for (String element: strings) {
				System.out.print(element + ", ");
			}
			System.out.println("");
		}

	}
}
