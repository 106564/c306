import java.io.File;
import java.io.StringReader;
import java.util.Iterator;
import java.util.Scanner;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XPathDemo {

	public static void main(String[] args) {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xPath = factory.newXPath();
		xPath.setNamespaceContext(new NamespaceContext() {
			@Override
			public String getNamespaceURI(String prefix) {
				String uri;
				if (prefix.equals("yweather"))
					uri = "http://xml.weather.yahoo.com/ns/rss/1.0";
				else if (prefix.equals("geo"))
					uri = "http://www.w3.org/2003/01/geo/wgs84_pos#";
				else
					uri = null;
				return uri;
			}

			@Override
			public Iterator<String> getPrefixes(String val) {
				return null;
			}

			@Override
			public String getPrefix(String uri) {
				return null;
			}

		});

		File xmlDocument = new File("bookmarks.xml");

		try {

			// TODO : P15 Task 1 - Change the XPath Expression and Determine the
			// Output
			String expression = "//title";

			String source = fileToString(xmlDocument);
			InputSource inputSource = new InputSource(new StringReader(source));
			XPathExpression xPathExpression = xPath.compile(expression);
			NodeList nodes = (NodeList) xPathExpression.evaluate(inputSource,
					XPathConstants.NODESET);

			for (int i = 0; i < nodes.getLength(); i++) {
				Node item = nodes.item(i);
				System.out.println(item.getTextContent());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("End of Program");
	}

	public static String fileToString(File file) throws Exception {
		String text = new Scanner(file).useDelimiter("\\Z").next();

		return text;
	}
}