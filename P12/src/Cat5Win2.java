import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class Cat5Win2 {
	private final int MAX = 5;

	private JTextField[] txtColumn = new JTextField[MAX];
	private JEditorPane pnlHtml = new JEditorPane();

	public Cat5Win2() {
		JFrame win = new JFrame("Category Five Sequel");

		win.setBounds(50, 50, 600, 500);
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		GridBagLayout gb = new GridBagLayout();
		JPanel pnlInputs = new JPanel(gb);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;

		for (int i = 0; i < MAX; i++) {
			JLabel label = new JLabel("Item " + (i + 1) + ": ",
					SwingConstants.RIGHT);
			gbc.gridwidth = 1;
			gbc.weightx = 0.0;
			gb.setConstraints(label, gbc);
			pnlInputs.add(label);

			txtColumn[i] = new JTextField(10);
			gbc.gridwidth = GridBagConstraints.REMAINDER;
			gbc.weightx = 1.0;
			gb.setConstraints(txtColumn[i], gbc);
			pnlInputs.add(txtColumn[i]);
		}

		JButton btnClear = new JButton("Clear Data");
		JButton btnLinks = new JButton("Retrieve Links");

		JPanel pnlButtons = new JPanel();
		pnlButtons.add(btnClear);

		pnlButtons.add(btnLinks);

		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearTextFields();
			}
		});

		btnLinks.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				retrieveLinks();
			}
		});

		JPanel pnlData = new JPanel(new BorderLayout());
		pnlData.setBorder(BorderFactory.createTitledBorder("Categories"));

		pnlData.add(pnlInputs, BorderLayout.NORTH);
		pnlData.add(pnlButtons, BorderLayout.SOUTH);

		JPanel pnlLink = new JPanel(new BorderLayout());
		pnlLink.setBorder(BorderFactory.createTitledBorder("Link"));

		pnlHtml.setEditable(false);
		pnlHtml.setContentType("text/html");
		pnlHtml.setText("");
		pnlHtml.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					goHyperLink(e.getURL().toString());
				}
			}
		});

		pnlLink.add(new JScrollPane(pnlHtml), BorderLayout.CENTER);

		win.add(pnlData, BorderLayout.NORTH);
		win.add(pnlLink, BorderLayout.CENTER);

		win.setVisible(true);
	}

	private void goHyperLink(String url) {
		try {
			Runtime.getRuntime().exec(
					"C:\\Program Files\\Internet Explorer\\iexplore.exe \""
							+ url + "\"");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void retrieveLinks() {
		StringBuilder sb = new StringBuilder();
		LinksBuilder lb = new LinksBuilder();
		for (int i = 0; i < MAX; i++) {
			if (!txtColumn[i].getText().trim().equals("")) {
				String tag = txtColumn[i].getText().trim();
				sb.append("<h3>Links for [" + tag + "]</h3>");
				try {
					String linksHtml = lb.getLinksHtml(lb.urlToString(tag));
					sb.append(linksHtml);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		pnlHtml.setText(sb.toString());
	}

	private void clearTextFields() {
		for (int i = 0; i < MAX; i++) {
			txtColumn[i].setText(null);
		}
		pnlHtml.setText(null);
	}

	public static void main(String[] args) {
		new Cat5Win2();
	}
}
