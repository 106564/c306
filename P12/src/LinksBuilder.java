import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class LinksBuilder {
	private XPath xPath = XPathFactory.newInstance().newXPath();
	
	public String getLinksHtml(String contents) {
		StringBuilder sb = new StringBuilder();
		List<String> titleList = new ArrayList<String>();
		List<String> linkList = new ArrayList<String>();

		try {

			// TODO: P12 Task 3 - Using XPath, get all relevant titles
			// Add these titles to titleList ArrayList
			String expression = "//title";

			InputSource inputSource = new InputSource(new StringReader(contents));
			XPathExpression xPathExpression = xPath.compile(expression);
			NodeList nodes = (NodeList) xPathExpression.evaluate(inputSource,
					XPathConstants.NODESET);

			for (int i = 0; i < nodes.getLength(); i++) {
				Node item = nodes.item(i);
				titleList.add(item.getTextContent());
				
			}

			// TODO: P12 Task 4 - Using XPath, get all relevant links
			// Add these links to linkList ArrayList
			String expression2 = "//link";

			InputSource inputSource2 = new InputSource(new StringReader(contents));
			XPathExpression xPathExpression2 = xPath.compile(expression2);
			NodeList nodes2 = (NodeList) xPathExpression2.evaluate(inputSource2,
					XPathConstants.NODESET);

			for (int i = 0; i < nodes2.getLength(); i++) {
				Node item = nodes2.item(i);
				linkList.add(item.getTextContent());
			}
			

			for (int i = 0; i < titleList.size(); i++) {
				sb.append("<a href='" + linkList.get(i) + "'>"
						+ titleList.get(i) + "</a><br/>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	public String urlToString(String tag) throws IOException {
		// TODO: P12 Task 2 - Create URL object
		URL url = new URL(tag);
				
		URLConnection connection = url.openConnection();
		InputStream inputStream = connection.getInputStream();
		String text = new Scanner(inputStream).useDelimiter("\\Z").next();
		return text;
	}
	
	
}
