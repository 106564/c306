import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

import java.io.File;
import java.util.Scanner;

import org.junit.Test;


public class LinksBuilderTest {
	@Test
	public void testGetLinksHtml() throws Exception {
		LinksBuilder builder = new LinksBuilder();

		String text = new Scanner(new File("feedTest.xml")).useDelimiter("\\Z").next();
		String result = builder.getLinksHtml(text);

		assertThat(result, containsString("<a href='http://www.sg'>Singapore</a>"));
		assertThat(result, containsString("<a href='http://www.rp.edu.sg'>Republic Poly</a>"));
	}

}
