import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

public class GUIMenuFourInLine {

	private static String[] menuItem = { "Start New Game",  };

	private static FourInLineBoard board;

	private static void init() {
		board = new FourInLineBoard();
	}

	private static void doOption(int choice) {
		int column = 0;
		switch (choice) {
		case 0:
			board.newGame();
			GUIMenuFourInLine.clear();
			displayBoard();
			
			boolean gameEnd = false;
			while (gameEnd == false) {
				column = GUIKeyboard.readInt("Enter column (0-6) for RED");
				board.placeMove(column, Move.RED);
				GUIMenuFourInLine.clear();
				displayBoard();
				if (board.checkWin(Move.RED)) {
					GUIKeyboard.display("RED Wins");
					board.newGame();
					GUIMenuFourInLine.clear();
					displayBoard();
					gameEnd = true;
					break;
				}
				column = GUIKeyboard.readInt("Enter column (0-6) for YELLOW");
				board.placeMove(column, Move.YELLOW);
				GUIMenuFourInLine.clear();
				displayBoard();
				if (board.checkWin(Move.YELLOW)) {
					GUIKeyboard.display("YELLOW Wins");
					board.newGame();
					GUIMenuFourInLine.clear();
					displayBoard();
					gameEnd = true;
					break;
				}
			}
			
			break;

//		case 1:
//			column = GUIKeyboard.readInt("Enter column (0-6) for RED");
//			board.placeMove(column, Move.RED);
//			GUIMenuFourInLine.clear();
//			displayBoard();
//			if (board.checkWin(Move.RED)) {
//				GUIKeyboard.display("RED Wins");
//				board.newGame();
//				GUIMenuFourInLine.clear();
//				displayBoard();
//			}
//			break;
//
//		case 2:
//			column = GUIKeyboard.readInt("Enter column (0-6) for YELLOW");
//			board.placeMove(column, Move.YELLOW);
//			GUIMenuFourInLine.clear();
//			displayBoard();
//			if (board.checkWin(Move.YELLOW)) {
//				GUIKeyboard.display("YELLOW Wins");
//				board.newGame();
//				GUIMenuFourInLine.clear();
//				displayBoard();
//			}
//			break;

		}
	}

	private static void displayBoard() {
		GUIMenuFourInLine.output("FOUR IN A LINE");
		GUIMenuFourInLine.output(board.toString());
		GUIMenuFourInLine.output("0 1 2 3 4 5 6");
	}

	public static void main(String[] args) {
		GUIMenuFourInLine.showMenu("Four In A Line");
	}

	//
	// DO NOT CHANGE ANY CODE FROM THIS POINT ONWARDS
	// UNLESS YOU UNDERSTAND WHAT YOU ARE DOING
	//
	private static JFrame win;
	private static DefaultListModel listModel = new DefaultListModel();
	private static JList jlist = new JList(listModel);

	public static void showMenu(String title) {
		win = new JFrame(title);
		win.setBounds(100, 100, 600, 400);

		// Create the Menu Option Buttons
		JPanel p1 = new JPanel(new GridLayout(menuItem.length, 1, 5, 5));
		p1.setBorder(new TitledBorder("Menu Items"));
		for (int i = 0; i < menuItem.length; i++) {
			JButton btn = new JButton(menuItem[i]);
			btn.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					String command = e.getActionCommand();
					for (int i = 0; i < menuItem.length; i++) {
						if (command.equals(menuItem[i])) {
							doOption(i);
							break;
						}
					}
				}
			});
			p1.add(btn);
		}
		win.add(p1, BorderLayout.NORTH);

		// Create the Display Area
		JPanel p2 = new JPanel(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(jlist);
		p2.setBorder(new TitledBorder("Display Area"));
		p2.add(scrollPane, BorderLayout.CENTER);
		jlist.setFont(new Font("Courier New", Font.PLAIN, 14));

		JPanel p3 = new JPanel(new FlowLayout());
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				listModel.clear();
			}
		});

		p3.add(btnClear);
		p2.add(p3, BorderLayout.SOUTH);
		win.add(p2, BorderLayout.CENTER);

		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);

		init();
	}

	public static void output(String line) {
		String[] lines = line.split("\n");
		for (int i = 0; i < lines.length; i++) {
			listModel.addElement(lines[i]);
		}
		jlist.setSelectedIndex(listModel.size() - 1);
		jlist.ensureIndexIsVisible(listModel.size() - 1);
	}

	public static void clear() {
		listModel.clear();
	}

}
