import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class ChessViewer {
	private ObjectMapper mapper;
	private JTextField fromField;
	private JTextField idField;
	private JTextField toField;
	private DefaultListModel moves;

	public ChessViewer() {
		mapper = new ObjectMapper();

		JFrame win = new JFrame("Chess Viewer");

		win.setBounds(50, 50, 600, 500);
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		win.add(buildMovePanel(), BorderLayout.NORTH);
		win.add(buildDisplayPanel(), BorderLayout.CENTER);

		win.pack();
		win.setVisible(true);
	}

	private Component buildDisplayPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder("Display"));

		moves = new DefaultListModel();
		panel.add(new JList(moves));

		return panel;
	}

	private Component buildMovePanel() {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder("Moves"));

		panel.add(new JLabel("ID"));
		idField = new JTextField(5);
		panel.add(idField);

		panel.add(new JLabel("From"));
		fromField = new JTextField(5);
		panel.add(fromField);

		panel.add(new JLabel("To"));
		toField = new JTextField(5);
		panel.add(toField);

		JButton moveButton = new JButton("Submit");
		moveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String id = idField.getText();
				String from = fromField.getText();
				String to = toField.getText();
				try {
					makeMove(id, from, to);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		panel.add(moveButton);

		return panel;
	}

	private void makeMove(String id, String from, String to) throws Exception {
		// TODO: P13 Task 3 - Create the URL and read the JSON data
		URL send = new URL("http://c306.herokuapp.com/" + id + "/" + from + "/" + to);
		urlToString(send);
		
		URL get = new URL("http://c306.herokuapp.com/" + id);
		String json = urlToString(get);
		
		// TODO: P13 Task 4 - Clear the moves data and then add in the moves
		// from the JSON
	
		Map<String, ArrayList> m = mapper.readValue(json, Map.class);
		System.out.println(m.get("moves").getClass());
		
		System.out.println(m.get("moves").get(0));
		
		Map lhm = new LinkedHashMap();
		String source;
		String destination;
		moves.clear();
		
		for (int i=0; i < m.get("moves").size(); i++) {
			lhm.clear();		
			lhm = (Map) m.get("moves").get(i);
			source = (String) lhm.get("source");
			destination = (String) lhm.get("destination");	
			
			moves.addElement(source + " - " + destination);
		}
	}

	private String urlToString(URL url) throws Exception {
		URLConnection connection;

		connection = url.openConnection();
		InputStream inputStream = connection.getInputStream();
		String text = new Scanner(inputStream).useDelimiter("\\Z").next();
		return text;
	}

	public static void main(String[] args) {
		new ChessViewer();
	}
}
