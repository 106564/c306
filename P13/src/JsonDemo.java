import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;


public class JsonDemo {
	public static void main(String[] args) throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		File person = new File("person.json");
		String personData = fileToString(person);
		Map<String, String> m = mapper.readValue(personData, Map.class);
		

		// TODO: P13 Task 1 - Read in JSON data using a Person class
		
		Person p = mapper.readValue(personData, Person.class);
		
		
		File phones = new File("phones.json");
		String phonesData = fileToString(phones);

		// TODO: P13 Task 2 - Read in JSON data for phones
		// Note for the 2nd part of this task
		// you will need to use new TypeReference<List<Phone>>() {}) for the 2nd parameter
		
		List<Phone> ph = mapper.readValue(phonesData,new TypeReference<List<Phone>>() {});	
		System.out.println(ph.get(0).getNumber());
	}

	public static String fileToString(File file) {
		String text= "";
		try {
			text = new Scanner(file).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return text;
	}
}
