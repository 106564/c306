import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class JsonReader {
	public String read(URL url) throws IOException {
		URLConnection connection = url.openConnection();
		InputStream inputStream = connection.getInputStream();
		String text = new Scanner(inputStream).useDelimiter("\\Z").next();
		return text;
	}

	public static void main(String[] args) throws Exception {
		JsonReader r = new JsonReader();
		String s = r.read(new URL("http://c306.herokuapp.com/2"));
		System.out.println(s);
	}
}
