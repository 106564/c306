import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GameServer {
	public static final int MAX_PLAYERS = 5;

	private ArrayList<Player> playerList;

	public GameServer() {
		playerList = new ArrayList<Player>();
	}

	public boolean isServerEmpty() {
		return playerList.isEmpty();
	}

	public boolean isServerFull() {
		return playerList.size() == MAX_PLAYERS;
	}

	public void logon(Player player) {
		if (playerList.isEmpty()) {
			playerList.add(player);
		} else {
			int index = new Random().nextInt(playerList.size());

			playerList.add(index, player);
		}

	}

	public void logout() {
		if (playerList.isEmpty()) {
			return;
		}
		int index = new Random().nextInt(playerList.size());
		playerList.remove(index);
	}

	public List<Player> getPlayers() {
		return Collections.unmodifiableList(playerList);
	}

}
