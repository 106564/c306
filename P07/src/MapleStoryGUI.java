import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class MapleStoryGUI {

	private GameServer gServer;
	private Queue gQueue;

	private JButton btnLogin = new JButton("Login");
	private JButton btnLogout = new JButton("Logout Any Player");

	private JTextField txtName = new JTextField(10);
	private JList lstServerPlayers;
	private JList lstQueuedPlayers;

	public MapleStoryGUI() {
		startServer();
		buildUI();
	}

	private void startServer() {
		gServer = new GameServer();
		//gServer.logon(new Player("Ruf"));
		//gServer.logon(new Player("Elektrawins"));
		//gServer.logon(new Player("MercifulJustice"));

		gQueue = new Queue();
	}

	private void buildUI() {

		final JFrame window = new JFrame("Maple Story Simulation");
		window.setLocation(100, 100);
		window.setSize(600, 400);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLayout(new GridLayout(1, 2));

		// Create the ONLINE PLAYERS panel
		JPanel pnlOnlinePlayer = new JPanel(new BorderLayout());
		pnlOnlinePlayer.setBorder(BorderFactory
				.createTitledBorder("Players Currently Online"));
		lstServerPlayers = new JList(gServer.getPlayers().toArray());
		JScrollPane spServer = new JScrollPane(lstServerPlayers);
		pnlOnlinePlayer.add(new JLabel("Max Player Limit : 5"),
				BorderLayout.NORTH);
		pnlOnlinePlayer.add(spServer, BorderLayout.CENTER);
		pnlOnlinePlayer.add(btnLogout, BorderLayout.SOUTH);

		btnLogout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				playerLogout();
			}
		});

		// Create the LOGIN panel
		JPanel pnlLogin = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pnlLogin.setBorder(BorderFactory.createTitledBorder("Login"));
		pnlLogin.add(new JLabel("User Name : "));
		pnlLogin.add(txtName);
		pnlLogin.add(btnLogin);

		btnLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String pName = txtName.getText();
				playerLogin(pName);
				txtName.setText("");
				if (isQueueFull() == true) {
					JOptionPane.showMessageDialog(window, "Queue is currently at maximum!");
				}
			}
		});

		// Create the QUEUED PLAYERS panel
		JPanel pnlQueuedPlayer = new JPanel(new BorderLayout());
		pnlQueuedPlayer.setBorder(BorderFactory
				.createTitledBorder("Players Currently Queueing"));
		lstQueuedPlayers = new JList(gQueue.getList().toArray());
		JScrollPane spPlayer = new JScrollPane(lstQueuedPlayers);
		pnlQueuedPlayer.add(
				new JLabel("first in queue", SwingConstants.CENTER),
				BorderLayout.NORTH);
		pnlQueuedPlayer.add(spPlayer, BorderLayout.CENTER);
		pnlQueuedPlayer.add(new JLabel("last in queue", SwingConstants.CENTER),
				BorderLayout.SOUTH);

		// Create the RIGHT panel for LOGIN and QUEUED PLAYERS panel
		JPanel pnlLoginAndQueuedPlayer = new JPanel(new BorderLayout());
		pnlLoginAndQueuedPlayer.add(pnlLogin, BorderLayout.NORTH);
		pnlLoginAndQueuedPlayer.add(pnlQueuedPlayer, BorderLayout.CENTER);

		window.add(pnlOnlinePlayer);
		window.add(pnlLoginAndQueuedPlayer);
		//window.pack();
		window.setVisible(true);

	}

	private void playerLogin(String name) {
		// TODO: P09 Task 9 - Code Player Login
		if (gServer.isServerFull() == true) {
			gQueue.enqueue(new Player(name));
		} else {
			gServer.logon(new Player(name));
		}
		refreshListBoxes();
	}

	private void playerLogout() {
		// TODO: P09 Task 10 - Code Player Logout
		gServer.logout();
		if (gServer.isServerFull() == false) {
			gServer.logon(gQueue.dequeue());
		}
		
		refreshListBoxes();
	}
	
	private boolean isQueueFull() {
		if (gQueue.isFull() == true) {
			return true;
		}
		return false;
	}

	private void refreshListBoxes() {
		lstQueuedPlayers.setListData(gQueue.getList().toArray());
		lstServerPlayers.setListData(gServer.getPlayers().toArray());
	}

	public static void main(String[] args) {
		new MapleStoryGUI();
	}
}
