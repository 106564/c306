public class Player {
	private static int nextID = 1;

	private int id;
	private String name;

	public Player(String name) {
		id = nextID++;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Player " + id + " : " + name;
	}
}