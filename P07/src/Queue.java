import java.util.ArrayList;

public class Queue {
	public static final int MAX = 10;
	
	private ArrayList<Player> list;
	
	Player[] thePlayers = new Player[MAX];
	int position = 0;

	
	public Queue() {
		list = new ArrayList<Player>();
	}
	
	public boolean enqueue(Player p) {
		// TODO : P09 Task 1 - Implement enqueue method
		if (isFull() == true) {
			return false;
		}
		//list.add(p);
		thePlayers[position] = p;
		position++;
		return true;
	}
	
	private Player[] moveArray() {
		Player[] temp  = new Player[MAX];
		int counter = 0;
		for (int i=1;i<thePlayers.length-1;i++) {
			temp[counter] = thePlayers[i];
			counter++;
		}
		return temp;
	}
	
	public Player dequeue() {
		// TODO : P09 Task 2 - Implement dequeue method
		Player player;
		if (isEmpty() == false) {
			//player = list.get(0);
			//list.remove(0);
			thePlayers = moveArray();
			player = thePlayers[0];
			return player;
		}
		return null;
	}


	public Player peek() {
		// TODO : P09 Task 3 - Implement peek method
		Player player;
		if (isEmpty() == false) {
			player = list.get(0);
			return player;
		}
		return null;
	}

	public int size() {
		// TODO : P09 Task 4 - Implement size method
		return list.size();
	}

	public boolean isEmpty() {
		// TODO : P09 Task 5 - Implement isEmpty method
		if (list.size() == 0) {
			return true;
		} 
		return false;
	}

	public boolean isFull() {
		// TODO : P09 Task 6 - Implement isFull method
		if (list.size() == MAX) {
			return true;
		}
		return false;
	}

	public ArrayList<Player> getList() {
		return list;
	}

	@Override
	public String toString() {
		String line = "";
		for (Player p : list) {
			line = line + p.getName() + " ";
		}
		return line;
	}
}
