public class QueueTry {
	public static void main(String[] args) {
		Queue queue1 = new Queue();
		Queue queue2 = new Queue();
		Queue queue3 = new Queue();

		// TODO : P09 Task 6 - Create Queue 1

		// Display Queue 1
		queue1.enqueue(new Player("A"));
		queue1.enqueue(new Player("B"));
		queue1.enqueue(new Player("X"));
		queue1.enqueue(new Player("Y"));
		System.out.println("Queue1 : " + queue1);

		// Create Queue 2
		queue2.enqueue(new Player("Z"));

		// Display Queue 2
		System.out.println("Queue2 : " + queue2);

		// TODO : P09 Task 7 - Create Queue 3
		queue3.enqueue(new Player("B"));
		queue3.enqueue(new Player("C"));
		queue3.enqueue(new Player("A"));

		// Display Queue 3
		System.out.println("Queue3 : " + queue3);

		// TODO : P09 Task 8 - Manipulate Queues 1-3 to achieve the desired
		// outcome
		
		queue2.enqueue(queue1.dequeue());
		queue2.enqueue(queue3.dequeue());
		queue2.enqueue(queue3.dequeue());
		queue1.enqueue(queue2.dequeue());
		queue1.enqueue(queue3.dequeue());
		queue3.enqueue(queue1.dequeue());
		queue3.enqueue(queue1.dequeue());
		queue3.enqueue(queue1.dequeue());
		
		
		// Display Queue 1, 2, 3
		System.out.println("A F T E R");
		System.out.println("Queue1 : " + queue1);
		System.out.println("Queue2 : " + queue2);
		System.out.println("Queue3 : " + queue3);

	}

}
