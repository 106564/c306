import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class QueueTest {
	private Queue queue;
	private Player player;

	@Before
	public void setUp() throws Exception {
		queue = new Queue();
		player = new Player("player");
	}

	@Test
	public void testEnqueue() {
		assertThat(queue.enqueue(player), is(true));

		assertThat(queue.size(), is(1));
		assertThat(queue.peek(), is(player));
	}

	@Test
	public void testEnqueueFullQueue() {
		for (int i = 0; i < Queue.MAX; i++) {
			assertThat(queue.enqueue(player), is(true));
		}

		assertThat(queue.enqueue(player), is(false));
	}

	@Test
	public void testDequeueEmptyQueue() {
		assertThat(queue.dequeue(), is(nullValue()));
	}

	@Test
	public void testDequeue() {
		queue.enqueue(player);
		assertThat(queue.dequeue(), is(player));
	}

	@Test
	public void testPeekEmptyQueue() {
		assertThat(queue.peek(), is(nullValue()));
	}

	@Test
	public void testPeek() {
		queue.enqueue(player);
		assertThat(queue.peek(), is(player));
	}

	@Test
	public void testSizeEmptyQueue() {
		assertThat(queue.size(), is(0));
	}

	@Test
	public void testSize() {
		queue.enqueue(new Player("nobody"));

		assertThat(queue.size(), is(1));

		queue.enqueue(player);

		assertThat(queue.size(), is(2));
	}

	@Test
	public void testIsEmptyForEmptyQueue() {
		assertThat(queue.isEmpty(), is(true));
	}

	@Test
	public void testIsEmpty() {
		queue.enqueue(new Player("nobody"));

		assertThat(queue.isEmpty(), is(false));

		queue.enqueue(player);

		assertThat(queue.isEmpty(), is(false));
	}

	@Test
	public void testIsFullEmptyQueue() {
		assertThat(queue.isFull(), is(false));
	}

	@Test
	public void testIsFull() {
		for (int i = 0; i < Queue.MAX; i++) {
			assertThat(queue.enqueue(player), is(true));
		}

		assertThat(queue.isFull(), is(true));
	}

}
