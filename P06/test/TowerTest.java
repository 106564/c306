import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;


public class TowerTest {
	private Tower tower;

	@Before
	public void setup() {
		tower = new Tower("Tower");
	}

	@Test
	public void testTopDiscWithEmptyTower() {
		assertThat(tower.topDisc(), is(nullValue()));
	}

	@Test
	public void testPlaceValidDisc() {
		assertThat(tower.placeDisc(Disc.SIZE4), is(true));
		assertThat(tower.topDisc(), is(Disc.SIZE4));
	}

	@Test
	public void testPlace2ValidDiscs() {
		tower.placeDisc(Disc.SIZE4);
		assertThat(tower.placeDisc(Disc.SIZE2), is(true));
		assertThat(tower.topDisc(), is(Disc.SIZE2));
	}

	@Test
	public void testPlaceInvalidDisc() {
		tower.placeDisc(Disc.SIZE2);

		assertThat(tower.placeDisc(Disc.SIZE4), is(false));
		assertThat(tower.topDisc(), is(Disc.SIZE2));
	}

	@Test
	public void testRemoveDiscFromEmptyTower() {
		assertThat(tower.removeDisc(), is(false));
	}

	@Test
	public void testRemoveDiscFromTower() {
		tower.placeDisc(Disc.SIZE3);

		assertThat(tower.removeDisc(), is(true));
		assertThat(tower.topDisc(), is(nullValue()));
	}
}
