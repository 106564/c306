import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class StackTest {
	private static final String TITLE = "title";
	private Stack stack;

	@Before
	public void setUp() {
		stack = new Stack(TITLE);
	}

	@Test
	public void testPushOne() {
		stack.push(Disc.SIZE4);

		assertThat(stack.size(), is(1));
		assertThat(stack.peek(), is(Disc.SIZE4));
	}

	@Test
	public void testPushNull() {
		stack.push(null);

		assertThat(stack.size(), is(0));
	}

	@Test
	public void testPopEmptyStack() {
		Disc result = stack.pop();

		assertNull(result);
	}

	@Test
	public void testPopStackWithOneItem() {
		stack.push(Disc.SIZE3);

		assertThat(stack.pop(), is(Disc.SIZE3));
	}

	@Test
	public void testPopStackWithTwoItem() {
		stack.push(Disc.SIZE3);
		stack.push(Disc.SIZE4);

		assertThat(stack.pop(), is(Disc.SIZE4));
		assertThat(stack.pop(), is(Disc.SIZE3));
	}

	@Test
	public void testPeekEmptyStack() {
		assertNull(stack.peek());
	}

	@Test
	public void testPeekStackWithItems() {
		stack.push(Disc.SIZE3);

		assertThat(stack.peek(), is(Disc.SIZE3));

		stack.push(Disc.SIZE4);

		assertThat(stack.peek(), is(Disc.SIZE4));
	}

	@Test
	public void testEmptyStackSize() {
		assertThat(stack.size(), is(0));
	}

	@Test
	public void testOneStackSize() {
		stack.push(Disc.SIZE4);

		assertThat(stack.size(), is(1));

		stack.push(Disc.SIZE4);

		assertThat(stack.size(), is(2));
	}

	@Test
	public void testEmptyStackIsEmpty() {
		assertThat(stack.isEmpty(), is(true));
	}

	@Test
	public void testOneStackIsNotEmpty() {
		stack.push(Disc.SIZE4);

		assertThat(stack.isEmpty(), is(false));
	}

	@Test
	public void testGetTitle() {
		assertThat(stack.getTitle(), is(TITLE));
	}

	@Test
	public void testGetListForEmptyStack() {
		assertThat(stack.getList().size(), is(0));
	}

	@Test
	public void testGetListForStackWithItems() {
		stack.push(Disc.SIZE5);
		stack.push(Disc.SIZE4);

		ArrayList<Disc> list = stack.getList();
		assertThat(list.size(), is(2));
		assertThat(list, hasItem(Disc.SIZE5));
		assertThat(list, hasItem(Disc.SIZE4));
	}

}
