import java.util.ArrayList;

public class Stack {
	private static final int TOP = 0;
	private ArrayList<Disc> list;
	private String title;

	public Stack(String title) {
		list = new ArrayList<Disc>();
		this.title = title;
	}

	public void push(Disc disc) {
		// TODO: P07 Task 1 - Add Disc to the FIRST item of the list
		if(disc != null) {
			list.add(0,disc);
		} 
	}

	public Disc pop() {
		// TODO: P07 Task 2 - Implement pop
		// A. Get the Disc from the FIRST item of the list
		// B. Remove the FIRST item of the list
		// C. Return the Disc which is gotten in A.
		if (list.isEmpty() == false) {
			Disc first = list.get(0);
			list.remove(0);
			return first;
		} else{
		return null;
			}
	}

	public Disc peek() {
		// TODO: P07 Task 3 - Implement peek
		// A. Get the Disc from the FIRST item of the list
		// B. Return the Disc which is gotten in A
		if (list.isEmpty() == false) {
			Disc first = list.get(0);
			return first;
		} else {
		return null;
			}
	}

	public int size() {
		// TODO: P07 Task 4 - Implement size
		// A. Return the size of the list
		return list.size();
	}

	public boolean isEmpty() {
		// TODO: P07 Task 5 - Implement empty
		// A. Check if size is zero
		// B. Do the obvious
		if (list.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	public String getTitle() {
		return title;
	}

	public ArrayList<Disc> getList() {
		return list;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(title + "\n");
		for (Disc c : list) {
			sb.append(c.toString() + "\n");
		}
		return sb.toString();
	}

}
