public class Tower {
	private Stack pile;
	private static int MAXHEIGHT = 5;

	public Tower(String title) {
		pile = new Stack(title);
	}

	public Disc topDisc() {
		return pile.peek();
	}

	public boolean placeDisc(Disc disc) {
		// TODO: P07 Task 9 - Implement placeDisc
		if (pile.isEmpty()) {
			pile.push(disc);
			return true;
		} else {
			if (disc.getRadius() < topDisc().getRadius()) {
				pile.push(disc);
				return true;
			}
			
		return false;
		}	
	}

	public boolean removeDisc() {
		if (pile.isEmpty()) {
			return false;
		}

		pile.pop();
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-11s", pile.getTitle()) + "\n");
		for (int i = 0; i < MAXHEIGHT - pile.size(); i++) {
			sb.append("     |     \n");
		}
		for (Disc d : pile.getList()) {
			sb.append(d.toString() + "\n");
		}
		sb.append("===========\n\n");
		return sb.toString();
	}

}
